<%@page pageEncoding="UTF-8" %>
<nav class="navbar navbar-default navbar-inverse" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">NLPLAB</a>
        </div>
        <div class="nav-collapse">
            <ul class="nav navbar-nav navbar-left">
                <li class="active"><a href="/">Home</a></li>
                <li><a href="#">About</a></li>
                <li><a href="/preUpload">上传</a></li>
            </ul>
        </div>
        <div class="nav-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="/invo/session/index">Log In/Sign Up</a></li>
            </ul>
        </div>
    </div>
</nav>