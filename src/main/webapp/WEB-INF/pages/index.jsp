<%@page pageEncoding="UTF-8" %>
<html>
<head>
    <meta charset="utf-8">
    <title>NlpLab | Welcome</title>
    <jsp:include page="layout/header.jsp"/>
</head>
<body>
<div class="wrap">
    <jsp:include page="layout/nav.jsp"/>
    <div class="container">
        <div class="alert alert-info">欢迎体验模糊谱聚类平台，在这里，您将直观的看到整个聚类过程。</div>


        <div class="jumbotron">
            <h1>欢迎！</h1>

            <p>本实验平台用图表的形式展示模糊谱聚类算法的聚类结果，并将聚类过程中的一些中间结果也展示出来。</p>

            <p><a href="/nlp/register" class="btn btn-primary btn-large btn-success">Try it for Free &raquo;</a></p>
        </div>

        <div class="row">
            <div class="col-md-4">
                <h2>预处理</h2>

                <p>Create, track and export your invoices online. Automate recurring invoices and design your own
                    invoice
                    using our invoice template and brand it with your business logo. </p>
                <p><a class="btn btn-default" href="http://www.yiiframework.com/forum/">预处理 &raquo;</a></p>
            </div>
            <div class="col-md-4">
                <h2>相似度计算</h2>

                <p>Gain critical insights into how your business is doing. See what sells most, who are your top paying
                    customers and the average time your customers take to pay.</p>
                <p><a class="btn btn-default" href="http://www.yiiframework.com/forum/">相似度计算 &raquo;</a></p>
            </div>
            <div class="col-md-4">
                <h2>模糊谱聚类</h2>

                <p>Invite users and share your workload as invoice supports multiple users with different permissions.
                    It
                    helps your business to be more productive and efficient. </p>
                <p><a class="btn btn-default" href="http://www.yiiframework.com/forum/">模糊谱聚类 &raquo;</a></p>
            </div>
        </div>
    </div>
</div>
<jsp:include page="layout/footer.jsp"/>
</body>
</html>
