<%@ page import="java.util.List" %>
<%@ page import="java.util.Map" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<header>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>查看分词结果</title>
    <jsp:include page="layout/header.jsp"/>
    <style type="text/css">
        body {
            margin: auto;
            width: 1200px;
        }

        .mod_col_text {
            float: left;
            width: 430px;
            margin-left: 30px;
        }

        .col_2_wrap .mod_col_h2,
        .col_2_wrap .mod_col_title {
            color: #2F6271;
        }

        .mod_col_title {
            padding-bottom: 10px;
        }

        .mod_col_title {
            height: 28px;
            line-height: 28px;
            font-weight: normal;
            padding-bottom: 16px;
        }

        .analysis_wrap {
            position: relative;
            width: 371px;
        }

        .analysis_content {
            height: 420px;
        }

        .analysis_content {
            position: relative;
            width: 350px;
            padding: 10px 6px 10px 15px;
            height: 532px;
            line-height: 24px;
            font-size: 14px;
            border-radius: 3px;
            background-color: rgba(255, 255, 255, 0.9);
            resize: none;
            overflow: hidden;
            outline: medium none;
        }

        .analysis_content .txt {
            min-height: 370px;
        }

        .analysis_content .txt {
            width: 98%;
            padding-right: 5px;
            min-height: 410px;
            padding-bottom: 45px;
            outline: medium none;
            border: 0px none;
            background: transparent none repeat scroll 0% 0%;
            overflow-y: auto;
            overflow-x: hidden;
        }

        .analysis_btn {
            position: absolute;
            bottom: 0px;
            width: 350px;
            padding: 6px 0px;
            text-align: right;
            background-color: rgba(255, 255, 255, 0.9);
            border-radius: 0px 0px 3px 3px;
        }

        .analysis_btn .replaced {
            float: left;
            margin-left: 10px;
            padding: 0px 4px;
            height: 24px;
            line-height: 24px;
            color: #2C2C2C;
            text-decoration: underline;
        }

        .analysis_btn .btn {
            display: inline-block;
            margin-right: 10px;
            padding: 0px 20px;
            height: 28px;
            line-height: 28px;
            text-align: center;
            border: 1px solid #909090;
            border-radius: 3px;
            background-color: #FFF;
            color: #575757;
            transition: all 0.4s ease-in-out 0s;
        }

        .col_2_wrap .mod_result_title {
            color: #FFF;
            font-weight: normal;
        }

        .mod_result_title {
            font-size: 16px;
            font-weight: bold;
            padding-bottom: 14px;
        }

        .result_tabs {
            overflow: hidden;
        }

        .result_tabs a:hover,
        .result_tabs a.current {
            background-color: #10667F;
            border: 2px solid #10667F;
        }

        .result_tabs a {
            float: left;
            margin-right: 10px;
            margin-bottom: 6px;
            height: 33px;
            line-height: 33px;
            padding: 0px 17px;
            font-size: 14px;
            border: 2px solid #FFF;
            border-radius: 4px;
            white-space: nowrap;
            color: #FFF;
            transition: all 0.4s cubic-bezier(0.3, 0, 0, 1) 0s;
        }

        .result_words {
            height: 350px;
            margin-top: -1px;
            padding: 6px 0px 6px 10px;
            background-color: rgba(255, 255, 255, 0.9);
            border-radius: 4px;
            color: #777;
        }

        .result_main .row {
            padding-bottom: 3px;
        }

        .mod_col_main {
            float: right;
            width: 640px;
            margin-right: 30px;
        }

        .col_bg {
            position: relative;
        }

        .ani_col_2 {
            animation: 3.6s ease-in-out 0.2s normal both 1 running ani_col_2;
        }

        .mCustomScrollBox {
            position: relative;
            overflow: auto;
            height: 100%;
            max-width: 100%;
            padding: 5px;
            outline: medium none;
            direction: ltr;
        }

        .mCSB_container {
            width: auto;
            height: auto;
            line-height: 26px;
        }

        .result_words .stress {
            background-color: #10667F;
        }

        .result_words .txt_bor {
            display: inline-block;
            margin: 0px 5px 10px;
            padding: 0px 8px;
            height: 29px;
            line-height: 29px;
            color: #FFF;
        }

        .result_words .normal {
            background-color: #A1A1A1;
        }

        .result_words .txt_bor {
            display: inline-block;
            margin: 0px 5px 10px;
            padding: 0px 8px;
            height: 29px;
            line-height: 29px;
            color: #FFF;
        }
    </style>
</header>

<body>
<div data-anchor="page2" style="height: 710px; background-color: rgb(73, 170, 199);" class="section wrap_xy_2 active" id="section1">
    <div class="col_2_wrap mod_col_wrap ui_maxw clearfix ani_col_1">
        <h2 class="mod_col_h2">词法类API
            <a class="col_help" href="help.cgi?topic=api#analysis" title="文档支持"><i class="icon_main icon_word"></i>文档支持</a>
        </h2>
        <div class="mod_col_text">
            <p class="mod_col_title">输入一段想分析的文字：</p>
            <div class="analysis_wrap">
                <div class="analysis_content">
                    <%List<String> fileNames=(List<String>)request.getAttribute("fileNames");%>
                    <%for(String fileName:fileNames){%>
                    <a href="/preprocess/getParticipleResult?fileName=<%=fileName%>"><%out.println(fileName+"<br/>");%></a>
                    <%}%>
                </div>
                <div class="analysis_btn">
                    <a class="replaced" href="#" title="换一段示例" id="sec1_replaced">换一段示例</a>
                    <a class="btn" href="#" title="清 除" id="sec1_clear">清 除</a>
                </div>
            </div>
        </div>
        <div class="mod_col_main result_main">
            <p class="mod_col_title">分析结果：</p>
            <dl class="row">
                <dt class="mod_result_title">词性</dt>
                <dd class="mod_result_content result_tabs" id="wtype_ret">
                    <a class="current" href="#" onclick="select_type(this)" title="时间词">时间词</a>
                    <a class="" href="#" onclick="select_type(this)" title="名词">名词</a>
                    <a href="#" onclick="select_type(this)" title="动词">动词</a>
                    <a href="#" onclick="select_type(this)" title="形容词">形容词</a>
                    <a href="#" onclick="select_type(this)" title="助词">助词</a>
                    <a href="#" onclick="select_type(this)" title="方位词">方位词</a>
                    <a href="#" onclick="select_type(this)" title="标点符号">标点符号</a>
                    <a href="#" onclick="select_type(this)" title="数词">数词</a>
                    <a href="#" onclick="select_type(this)" title="量词">量词</a>
                    <a href="#" onclick="select_type(this)" title="副词">副词</a>
                    <a href="#" onclick="select_type(this)" title="代词">代词</a>
                    <a href="#" onclick="select_type(this)" title="人名">人名</a>
                </dd>
            </dl>
            <dl class="row">
                <dt class="mod_result_title">分词</dt>
                <dd class="mod_result_content result_words mCustomScrollbar _mCS_2 _mCS_1 mCS_no_scrollbar">
                    <div tabindex="0" id="mCSB_1" class="mCustomScrollBox mCS-light mCSB_vertical mCSB_inside">
                        <div id="mCSB_1_container" class="mCSB_container mCS_y_hidden mCS_no_scrollbar_y" style="position:relative; top:0; left:0;" dir="ltr">
                            <div id="mCSB_2" class="mCustomScrollBox mCS-light mCSB_vertical mCSB_inside" tabindex="0">
                                <div id="mCSB_2_container" class="mCSB_container mCS_y_hidden mCS_no_scrollbar_y" style="position:relative; top:0; left:0;" dir="ltr">
                                    <%Map<String,String> resultMap= (Map<String, String>) request.getAttribute("resultMap");%>
                                    <%if(resultMap!=null){%>
                                    <%for(String key:resultMap.keySet()){%>
                                        <%if (resultMap.get(key).equals("时间词")){%>
                                            <span class="txt_bor stress" option-data="<%=resultMap.get(key)%>"><%=key%></span>
                                        <%}else{%>
                                            <span class="txt_bor normal" option-data="<%=resultMap.get(key)%>"><%=key%></span>
                                        <%}%>
                                    <%}}%>
                                </div>
                                <div style="display: none;" id="mCSB_2_scrollbar_vertical" class="mCSB_scrollTools mCSB_2_scrollbar mCS-light mCSB_scrollTools_vertical">
                                    <div class="mCSB_draggerContainer">
                                        <div id="mCSB_2_dragger_vertical" class="mCSB_dragger" style="position: absolute; min-height: 30px; height: 0px; top: 0px;" oncontextmenu="return false;">
                                            <div style="line-height: 30px;" class="mCSB_dragger_bar"></div>
                                        </div>
                                        <div class="mCSB_draggerRail"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div style="display: none;" id="mCSB_1_scrollbar_vertical" class="mCSB_scrollTools mCSB_1_scrollbar mCS-light mCSB_scrollTools_vertical">
                            <div class="mCSB_draggerContainer">
                                <div id="mCSB_1_dragger_vertical" class="mCSB_dragger" style="position: absolute; min-height: 30px; height: 0px; top: 0px;" oncontextmenu="return false;">
                                    <div style="line-height: 30px;" class="mCSB_dragger_bar"></div>
                                </div>
                                <div class="mCSB_draggerRail"></div>
                            </div>
                        </div>
                    </div>
                </dd>
            </dl>
        </div>
    </div>
</div>
<jsp:include page="layout/footer.jsp"/>
<script type="text/javascript">
    function select_type(el) {
        var type = $(el).html();
        $('.mod_result_content .current').removeClass('current');
        $(el).addClass('current');
        $('.stress').removeClass('stress').addClass('normal');
        $('[option-data="' + type + '"]').removeClass('normal').addClass('stress');
    }
//    function getParticipleResult(fileName)
//    {
//        $.ajax({
//            type: "get", //表单提交类型
//            url: "/preprocess/getParticipleResult?fileName="+fileName, //表单提交目标
//            //dataType: 'json',
//            success: function (msg) {
//                $('#mCSB_2_container').html('lala');
//            }
//        });
//    }
</script>
</body>

</html>
