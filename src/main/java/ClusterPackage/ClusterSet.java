package ClusterPackage;

/**
 * Created by LZG on 2016/1/14.
 */
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.File;

// 聚簇集存储最终聚类结果
class ClusterSet {
    int c = 0;//调用计数器

    // 聚簇链表
    private ArrayList<HashSet<Integer>> clusterList = new ArrayList<HashSet<Integer>>();

    // 已处理结点集，便于查询未添加进聚簇集的孤立结点
    private HashSet<Integer> ProcessedSet = new HashSet<Integer>();

    // 向聚簇集中添加一条边，处理这条边的两个节点
    public void addCluster(MSTEdge edge) {
        int cluster1 = -1;// cluser=-1表示未找到前驱结点
        int cluster2 = -1;// cluser=-1表示未找到后继结点
        try {
            // Iterator<HashSet<Integer>> iterator = clusterList.iterator();
            //boolean find = false;// 是否寻找到添加位置
            c++;
            System.out.println(edge.getPreNode() + "-" + edge.getSucNode());
            //int size=clusterList.size();
            for (int i = 0; i < clusterList.size()&&(cluster1==-1||cluster2==-1); i++) {
                HashSet<Integer> hs = clusterList.get(i);

                // 集合包含前驱结点，则添加后继结点
                if (hs.contains(edge.getPreNode()) && cluster1 == -1) {
                    cluster1 = i;
					/*
					 * if (!hs.contains(edge.getSucNode())) {
					 * hs.add(edge.getSucNode()); // clusterList.remove(i);//
					 * 删除原集合 clusterList.set(i, hs);// 添加后继节点后加入链表
					 * ProcessedSet.add(edge.getSucNode());// 添加进已处理结点集 find =
					 * true;// 标识已寻找 System.out.println(edge.getPreNode() + "-"
					 * + edge.getSucNode()); System.out.println("添加后继:" +
					 * edge.getSucNode() + "size" + clusterList.size()+"计数"+c);
					 * c++; //break;
					 *
					 * } else { System.out.println("错误:" + edge.getPreNode() +
					 * "-" + edge.getSucNode() + "已经存在"); e++; }
					 */
                } else if (hs.contains(edge.getSucNode()) && cluster2 == -1) {
                    cluster2 = i;
					/*
					 * if (!hs.contains(edge.getPreNode())) {
					 * hs.add(edge.getPreNode()); // clusterList.remove(i);//
					 * 删除原集合 clusterList.set(i, hs);// 添加后继节点后加入链表
					 * ProcessedSet.add(edge.getPreNode());// 添加进已处理结点集 find =
					 * true;// 标识已寻找 System.out.println(edge.getPreNode() + "-"
					 * + edge.getSucNode()); System.out.println("添加前驱:" +
					 * edge.getPreNode() + "size" + clusterList.size()+"计数"+c);
					 * c++; //break; } else { System.out.println("错误:" +
					 * edge.getPreNode() + "-" + edge.getSucNode() + "已经存在");
					 * e++; }
					 */
                }
            }// end for

            // 未寻找到添加位置，新建集合
            if (cluster1 == -1 && cluster2 == -1) {
                HashSet<Integer> hs = new HashSet<Integer>();
                hs.add(edge.getPreNode());
                hs.add(edge.getSucNode());
                ProcessedSet.add(edge.getPreNode());
                ProcessedSet.add(edge.getSucNode());
                clusterList.add(hs);
                //find=true;
                System.out.println("新建聚类:" + edge.getPreNode() + "-"
                        + edge.getSucNode() + " size:" + clusterList.size()
                        + " 计数:" + c);
            }

            // 同时发现前驱和后继
            // 将cluster2合并入cluster1
            else if (cluster1 != -1 && cluster2 != -1) {
                HashSet<Integer> hs1 = clusterList.get(cluster1);
                HashSet<Integer> hs2 = clusterList.get(cluster2);
                Iterator<Integer> iterator = hs2.iterator();

                while (iterator.hasNext()) {
                    hs1.add(iterator.next());// 将cluster2合并入cluster1
                }

                clusterList.set(cluster1, hs1);// 更新cluster1
                clusterList.remove(cluster2);// 删除cluster2

                //find=true;

                System.out.println("合并聚类:" + edge.getPreNode() + "-"
                        + edge.getSucNode() + " size:" + clusterList.size()
                        + " 计数:" + c);
            }

            // 发现前驱结点则添加后继结点
            else if (cluster1 != -1) {
                HashSet<Integer> hs = clusterList.get(cluster1);
                hs.add(edge.getSucNode());
                clusterList.set(cluster1, hs);// 添加后继节点后加入链表

                // clusterList.remove(i);// 删除原集合

                ProcessedSet.add(edge.getSucNode());// 添加进已处理结点集
                //find = true;// 标识已寻找
                //System.out.println(edge.getPreNode() + "-" + edge.getSucNode());
                System.out.println("添加后继:" + edge.getSucNode() + " size:"
                        + clusterList.size() + " 计数:" + c);
                // break;

            }

            // 发现后继结点则添加前驱结点
            else if (cluster2 != -1) {
                HashSet<Integer> hs = clusterList.get(cluster2);
                hs.add(edge.getPreNode());
                clusterList.set(cluster2, hs);// 添加后继节点后加入链表

                // clusterList.remove(i);// 删除原集合

                ProcessedSet.add(edge.getPreNode());// 添加进已处理结点集
                //find = true;// 标识已寻找
                //System.out.println(edge.getPreNode() + "-" + edge.getSucNode());
                System.out.println("添加前驱:" + edge.getPreNode() + " size:"
                        + clusterList.size() + " 计数:" + c);
                // break;

            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
            System.out
                    .println("cluster1:" + cluster1 + " cluster2:" + cluster2);

        }

		/*
		 * while (iterator.hasNext()) { HashSet<Integer> hs = iterator.next();
		 * int pre=edge.getPreNode(); int suc=edge.getSucNode(); //
		 * 集合包含前驱结点，则添加后继结点
		 *
		 * if (hs.contains(edge.getPreNode())) { clusterList.remove(hs); //
		 * 删除原集合 hs.add(edge.getSucNode()); clusterList.add(hs);// 添加后继节点后加入链表
		 * ProcessedSet.add(edge.getSucNode());// 添加进已处理结点集 find = true;// 标识已寻找
		 * break; }
		 *
		 * // 集合包含后继结点，则添加前驱结点 else if (hs.contains(edge.getSucNode())) {
		 * clusterList.remove(hs); hs.add(edge.getPreNode());
		 * clusterList.add(hs); ProcessedSet.add(edge.getPreNode()); find =
		 * true; break; }
		 *
		 * }// end while
		 *
		 *
		 * // 未寻找到添加位置，新建集合 if (find == false) { HashSet<Integer> hs = new
		 * HashSet<Integer>(); hs.add(edge.getPreNode());
		 * hs.add(edge.getSucNode()); ProcessedSet.add(edge.getPreNode());
		 * ProcessedSet.add(edge.getSucNode()); clusterList.add(hs); }
		 */
    }

    // 输出聚类结果
    public void outPutClusterSet(String path) {
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(new File(
                    path), false));
            // int size=clusterList.size();
            for (int i = 0; i < clusterList.size(); i++) {
                HashSet<Integer> hs = clusterList.get(i);
                Iterator<Integer> iterator = hs.iterator();
                String line = i + 1 + ":";
                while (iterator.hasNext()) {
                    line += "\t" + iterator.next();
                }
                writer.write(line);
                if (i != clusterList.size() - 1) {
                    writer.newLine();
                }
                writer.flush();
            }
            writer.close();
        } catch (Exception e) {
        }
    }

    // 查询孤立结点
    public void checkIsolatedNodes(int nodesNumber) {
        for (int i = 0; i < nodesNumber; i++) {
            if (!ProcessedSet.contains(i)) {
                HashSet<Integer> hs = new HashSet<Integer>();
                hs.add(i);
                clusterList.add(hs);
                ProcessedSet.add(i);
            }
        }
    }

}
