package ClusterPackage;

import org.junit.Test;

/**
 * Created by LZG on 2016/1/14.
 */



// 最小生成树边
class MSTEdge implements Comparable<Object> {
    private int preNode;
    private int sucNode;
    private double weight;

    public MSTEdge(int preNode, int sucNode, double weight) {
        this.preNode = preNode;
        this.sucNode = sucNode;
        this.weight = weight;
    }

    public int getPreNode() {
        return this.preNode;
    }

    public int getSucNode() {
        return this.sucNode;
    }

    public double getWeight() {
        return this.weight;
    }

    public int compareTo(Object obj) {
        MSTEdge edge = (MSTEdge) obj;
        return new Double(this.getWeight()).compareTo(edge.getWeight());
    }
}


public class TextClustering {
    /**
     * @param args
     */
    @Test
    public void main_sim() {
        // 进行建模

        ModelClass modelBuilder=new ModelClass();
        modelBuilder.buildModel();
    }

}