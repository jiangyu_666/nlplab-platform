package ClusterPackage;

import org.springframework.stereotype.Controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by LZG on 2016/1/14.
 */

// 路径类用于存储所有文件夹路径
// 设置预处理及聚类阶段路径存放
public class PathClass {

    // 文件夹路径设置
    // initialFolder = BasePath + InitialFolder;等等

    // 预处理文件夹
    public static String initialFolder;// 原始文件路径
    public static String filtedFolder;// 停用词过滤后的文本路径
    public static String statisticsFolder;// 词汇出现次数统计结果路径
    public static String finalTFFolder;// 词频统计路径
    public static String finalIDFFolder;// 文档频率统计路径
    public static String finalValueFolder;// 最终特征项权重路径
    public static String finalTextFolder;// 降维后的文本路径
    public static String filtFolder;// 停用词词典路径


    // 相似度计算文件夹
    public static String nodesFolder;// 文本结点集文件夹
    public static String edgesFolder;// 文本边集文件夹
    public static String mcgNodesFolder;// 最大公共子图结点集文件夹
    public static String mcgEdgesFolder;// 最大公共子图边集文件夹
    public static String similarityFolder;// 相似度文件夹
    public static String mstFolder;// 最小生成树文件夹


    // 文件夹路径设置
    // initialFolder = BasePath + InitialFolder;等等
    public static String BasePath;//基础路径
    public static String PlatFormPath;// 平台路径nlplab-platform
    // 预处理文件夹
    public static String InitialFolder = "//";// 原始文件路径
    public static String FiltedFolder = "//filtedText//";// 停用词过滤后的文本路径
    public static String StatisticsFolder = "//statistics//";// 词汇出现次数统计结果路径
    public static String FinalTFFolder = "//finalTF//";// 词频统计路径
    public static String FinalIDFFolder = "//finalIDF//";// 文档频率统计路径
    public static String FinalValueFolder = "//finalValue//";// 最终特征项权重路径
    public static String FinalTextFolder = "//finalText//";// 降维后的文本路径
    public static String FiltFolder ="//wordsFilt//";// 停用词词典路径

    // 相似度计算文件夹
    public static String NodesFolder = "//nodes//";// 文本结点集文件夹
    public static String EdgesFolder = "//edges//";// 文本边集文件夹
    public static String MCGNodesFolder = "//MCGNodes//";// 最大公共子图结点集文件夹
    public static String MCGEdgesFolder = "//MCGEdges//";// 最大公共子图边集文件夹
    public static String SimilarityFolder = "//similarity//";// 相似度文件夹
    public static String MSTFolder = "//MST//";// 最小生成树文件夹

    // session初始化
    // 用于平台
    public  void initialize(final HttpSession session){
        PlatFormPath=session.getServletContext().getRealPath("/");
        String prePath = PlatFormPath + "static/uploads/";
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        BasePath = prePath + dateFormat.format(new Date()) + "/" + session.getId() + "/";

        // 建立文件夹路径
        // 预处理路径
        initialFolder = BasePath + InitialFolder;
        filtedFolder = BasePath + FiltedFolder;
        statisticsFolder = BasePath + StatisticsFolder;
        finalTFFolder = BasePath + FinalTFFolder;
        finalIDFFolder = BasePath + FinalIDFFolder;
        finalValueFolder = BasePath + FinalValueFolder;
        finalTextFolder = BasePath + FinalTextFolder;
        filtFolder ="E://WorkSpace//nlplab-platform//target//nlplab-platform//static"+FiltFolder;

        //相似度计算路径
        nodesFolder = BasePath + NodesFolder;
        edgesFolder = BasePath + EdgesFolder;
        mcgNodesFolder = BasePath + MCGNodesFolder;
        mcgEdgesFolder = BasePath + MCGEdgesFolder;
        similarityFolder = BasePath + SimilarityFolder;
        mstFolder = BasePath + MSTFolder;

    }


    //无session初始化
    //用于测试
    public static void initialize(){
        // 建立基本路径
        BasePath ="E://WorkSpace//nlplab-platform//target//nlplab-platform//static//uploads//2016-01-17//034BEEF26EB06D4C2A925CCD9252CDD1";

        // 建立文件夹路径
        // 预处理路径
        initialFolder = BasePath + InitialFolder;
        filtedFolder = BasePath + FiltedFolder;
        statisticsFolder = BasePath + StatisticsFolder;
        finalTFFolder = BasePath + FinalTFFolder;
        finalIDFFolder = BasePath + FinalIDFFolder;
        finalValueFolder = BasePath + FinalValueFolder;
        finalTextFolder = BasePath + FinalTextFolder;
        filtFolder ="E://WorkSpace//nlplab-platform//target//nlplab-platform//static"+FiltFolder;

        //相似度计算路径
        nodesFolder = BasePath + NodesFolder;
        edgesFolder = BasePath + EdgesFolder;
        mcgNodesFolder = BasePath + MCGNodesFolder;
        mcgEdgesFolder = BasePath + MCGEdgesFolder;
        similarityFolder = BasePath + SimilarityFolder;
        mstFolder = BasePath + MSTFolder;
    }

    // 查询路径下所有文件
    // 返回文件名列表List
    public static List<String> findAllFiles(String path) {
        List<String> fileList = new ArrayList<String>();
        File fileDir = new File(path);

        // 获取文件名
        File[] files = fileDir.listFiles();

        System.out.println(files);
        for (int i = 0; i < files.length; i++) {
            // 如果路径不是文件夹
            if (!files[i].isDirectory()) {
                fileList.add(files[i].getName());
            }
        }// end for
        return fileList;
    }
    // 生成预处理文件夹
    public static void createFolder_pre(){
        createFolder(PathClass.initialFolder);
        createFolder(PathClass.filtedFolder);
        createFolder(PathClass.statisticsFolder);
        createFolder(PathClass.finalTFFolder);
        createFolder(PathClass.finalIDFFolder);
        createFolder(PathClass.finalValueFolder);
        createFolder(PathClass.finalTextFolder);
        //filedfolder是公用的固定文件夹不需要创建
    }

    // 生成相似度文件夹
    public static void createFolder_sim(){
        createFolder(PathClass.nodesFolder);
        createFolder(PathClass.edgesFolder);
        createFolder(PathClass.mcgNodesFolder);
        createFolder(PathClass.mcgEdgesFolder);
        createFolder(PathClass.similarityFolder);
        createFolder(PathClass.mstFolder);

    }

    //输入文件夹路径
    //如果不存在则生成文件夹
    public static void createFolder(String path){
        File file=new File(path);
        if(!file .exists()) {
            file.mkdir();
        }

    }

}
