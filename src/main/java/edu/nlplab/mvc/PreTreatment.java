package edu.nlplab.mvc;

/**
 * Created by LZG on 2015/7/28.
 */

import ClusterPackage.MyPretreatment;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;


@Controller
@RequestMapping("/")
public class PreTreatment {

    @RequestMapping(value = "/pre_treatment", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public void main_tmple(final ModelMap model, final HttpSession session) {
        long time0,time1,time2,time3,time4;

        // MyPretreatment pretreat=new MyPretreatment(session);
        MyPretreatment pretreat=new MyPretreatment(session);

        time0=System.currentTimeMillis(); //开始时间

        pretreat.ParagraphProcess();
        System.out.println("分词及过滤完成");

        time1=System.currentTimeMillis();//分词时间
        System.out.println("分词时间：" + (time1 - time0) + "ms");

        pretreat.statistics();
        System.out.println("统计完成");

        time2=System.currentTimeMillis();//统计时间
        System.out.println("统计时间：" + (time2 - time1) + "ms");

        pretreat.calculate();

        pretreat.calculateTF_IDF();
        System.out.println("词频计算完成");

        time3=System.currentTimeMillis();//TFIDF时间
        System.out.println("TFIDF时间：" + (time3 - time2) + "ms");

        pretreat.jiangWei();
        System.out.println("降维处理完成");

        time4=System.currentTimeMillis();//降维时间
        System.out.println("降维时间：" + (time4 - time3) + "ms");

        System.out.println("总时间：" + (time4 - time0) +  "ms");    //输出程序总运行时间
        model.addAttribute("msg1", "Segment success");

    }

}